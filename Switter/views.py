import requests
from django.shortcuts import render

# Create your views here.
from .models import Product


def index_page(request):
    products = Product.objects.all()
    return render(request, 'index_new.html', locals())

def product_detailed(request, product_id=None):
    product = Product.objects.get(id=product_id)

    order_sent = False

    if request.POST and request.POST.get('name'):
        order_sent = True
        client_name = request.POST.get('name')
        BOT_TOKEN = '5506387540:AAGUSoGpZyQy47KPkVvZX-E_1q7uH0svROc'

        req = requests.post(f'https://api.telegram.org/bot{BOT_TOKEN}/sendMessage', {
            'chat_id': -737376419,
            'text': f'\U0001F609 {client_name} заказал "{product} #заказы"',
            'parse_mode': 'html'
        })

        print(req.json())

    return render(request, 'product_detailed.html', locals())