# Create your models here.
from django.db import models


class Product(models.Model):
   class Meta:
       verbose_name = 'Товар'
       verbose_name_plural = 'Товары'

   name = models.CharField('Наименование', max_length=150)
   price = models.IntegerField('Цена', default=1)
   description = models.TextField('Описание', blank=True, null=True)

   image = models.ImageField('Картинка', upload_to='images/', blank=True, null=True)

   def __str__(self):
       return f'{self.name}'




